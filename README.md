## Maven

```xml
<repository>
  <id>andrei1058-repo</id>
  <url>http://repo.andrei1058.com/releases/</url>
</repository>
```
```xml
<dependency>
  <groupId>com.andrei1058.spigot.updater</groupId>
  <artifactId>update-checker</artifactId>
  <version>1.0</version>
</dependency>
```

Plugin update checker 
```
 new SpigotUpdater(pluginInstance, resourceID, sendUpdateFoundMessage).checkUpdate();
```